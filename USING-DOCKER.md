# Using docker-compose

## Setup

- To setup containers for API developmente, run:

```bash
docker-compose build
docker-compose up
```

**NOTE** These commands will **create a docker image for the API**, and load the database server.

## Running the API

- To run a command in a container, assuming `docker-compose up` was ran, run:

```bash
docker container exec [OPTIONS] CONTAINER COMMAND
```

- To run an interactive command, use the `-it` option

- CONTAINER can be the container id or the container name, both can be found running:

```bash
docker container ls
```

## Observations

- The `build` operation may take a while depending on your internet connection.