"""Merge remove blacklist and add pending field

Revision ID: c996d488053d
Revises: 9d5ffa257c80, d8a2b0e8ad32
Create Date: 2018-09-22 22:35:01.938615

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c996d488053d'
down_revision = ('9d5ffa257c80', 'd8a2b0e8ad32')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
