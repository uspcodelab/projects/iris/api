# app/__init__.py

from flask_restplus import Api
from flask import Blueprint

# Import controllers:
from .main.controller.central_controller import api as central_ns
from .main.controller.equipment_controller import api as equipment_ns
from .main.controller.department_controller import api as department_ns
from .main.controller.unit_controller import api as unit_ns
from .main.controller.equipment_search_controller import api as equipment_search_ns
from .main.controller.user_controller import api as user_ns
from .main.controller.auth_controller import api as auth_ns

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='FLASK RESTPLUS API BOILER-PLATE WITH JWT',
          version='1.0',
          description='a boilerplate for flask restplus web service'
          )

# Add API namespaces:
api.add_namespace(central_ns, path='/central')
api.add_namespace(equipment_ns, path='/equipment')
api.add_namespace(unit_ns, path='/unit')
api.add_namespace(department_ns, path='/department')
api.add_namespace(equipment_search_ns, path='/search')
api.add_namespace(user_ns, path='/user')
api.add_namespace(auth_ns)
