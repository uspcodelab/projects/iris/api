from .. import db, flask_bcrypt
import datetime
import jwt
from ..config import key

class User(db.Model):
    __tablename__ = 'users'

    # DB Relationships
    person = db.relationship("Person", backref='people', lazy=True, uselist=False)
    manages = db.relationship("Manager", lazy='dynamic')

    # System control section
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token_id = db.Column(db.Integer, nullable=False)
    registered_on = db.Column(db.DateTime(), nullable=False)

    # USP Bonding section
    is_USP_user = db.Column(db.Boolean, nullable=False)
    workplace = db.Column(db.String(64), nullable=False)
    USP_bond_options = db.Column(db.String(256), nullable=False)

    # ID Section
    user_CPF = db.Column(db.String(64), nullable=False)
    is_person = db.Column(db.Boolean, nullable=False)
    birth_date = db.Column(db.String(64), nullable=True)
    gender = db.Column(db.String(64), nullable=True)
    person_id = db.Column(db.String(64), nullable=True)
    company_name = db.Column(db.String(64), nullable=True)
    company_id = db.Column(db.String(64), nullable=True)
    company_ie = db.Column(db.String(64), nullable=True)
    company_im = db.Column(db.String(64), nullable=True)

    # Address
    user_CEP = db.Column(db.String(64), nullable=False)
    user_address = db.Column(db.String(64), nullable=False)
    billing_address = db.Column(db.String(64), nullable=False)
    fields_of_interest = db.Column(db.String(64), nullable=False)
    user_city = db.Column(db.String(64), nullable=False)
    user_state = db.Column(db.String(64), nullable=False)
    user_phone = db.Column(db.String(64), nullable=False)
    user_phone2 = db.Column(db.String(64), nullable=True)

    # Access data
    password_hash = db.Column(db.String(64), nullable=False)

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.password_hash = flask_bcrypt.generate_password_hash(
            password).decode('utf-8')

    def check_password(self, password):
        return flask_bcrypt.check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<User '{}'>".format(self.user_CPF)

    def encode_auth_token(self, user_id):
            """
            Generates the Auth Token
            :return: string
            """
            try:
                token_id = User.query.filter_by(id=user_id).first().token_id + 1
                User.query.filter_by(id=user_id).first().token_id += 1
                db.session.commit()
                payload = {
                    'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1, seconds=5),
                    'iat': datetime.datetime.utcnow(),
                    'sub': user_id,
                    'token_id': token_id
                }
                return jwt.encode(
                    payload,
                    key,
                    algorithm='HS256'
                )
            except Exception as e:
                return e

    @staticmethod  
    def decode_auth_token(auth_token):
        """
        Decodes the auth token
        :param auth_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(auth_token, key)
            token_id = User.query.filter_by(id=payload['sub']).first().token_id
            db.session.commit()
            # is_blacklisted_token = BlacklistToken.check_blacklist(auth_token)
            if payload['token_id'] != token_id:
                return 'Token blacklisted. Please log in again.'
            else:
                return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'

    # TODO: VER SE É COMITE GESTOR/PRESIDENTE OU SE É ADMIN DE EQUIPAMENTO
