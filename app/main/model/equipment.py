from .. import db

class Equipment(db.Model):
    __tablename__ = 'equipments'

    # Equipment identification:
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    registered_on = db.Column(db.DateTime(), nullable=False)

    # ID of the central associated to an equipment
    central_id = db.Column(db.Integer, db.ForeignKey('centrals.id'), nullable=False)

    # Relationship between equipments and a department
    department_id = db.Column(db.Integer, db.ForeignKey('departments.id'), nullable=False)

    #Relationship between equipments and People
    people = db.relationship('Person', secondary='equipment_related_roles')

    equipment_name = db.Column(db.String(64), nullable=False)
    equipment_english_name = db.Column(db.String(64), nullable=False)
    equipment_presentation = db.Column(db.String(1024), nullable=False)
    equipment_brand = db.Column(db.String(64), nullable=False)
    equipment_model = db.Column(db.String(64), nullable=False)
    equipment_patrimony_number = db.Column(db.String(10), nullable=False)
    equipment_acquisition_year = db.Column(db.String(4), nullable=False)
    # equipment_origin
    
    # Location:
    equipment_address = db.Column(db.String(64), nullable=False)
    #google maps address required
    
    # General description:
    equipment_description = db.Column(db.String(1024), nullable=False)
    equipment_english_description = db.Column(db.String(1024), nullable=False)
    equipment_offered_services = db.Column(db.String(1024), nullable=True)
    equipment_applications = db.Column(db.String(1024), nullable=True)
    equipment_rules = db.Column(db.String(1024), nullable=False)

    # Equipment attendance hours:
    monday_attendance_time = db.Column(db.String(256), nullable=True) 
    tuesday_attendance_time = db.Column(db.String(256), nullable=True)
    wednesday_attendance_time = db.Column(db.String(256), nullable=True)
    thursday_attendance_time = db.Column(db.String(256), nullable=True)
    friday_attendance_time = db.Column(db.String(256), nullable=True)
    saturday_attendance_time = db.Column(db.String(256), nullable=True)
    sunday_attendance_time = db.Column(db.String(256), nullable=True)

    #file upload required

    def __repr__(self):
        return "<Equipment '{}'>".format(self.equipment_name)