from .. import db

class Central(db.Model):
    """ Central model for storing central related details """
    __tablename__ = 'centrals'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    registered_on = db.Column(db.DateTime(), nullable=False)

    # Relationship definition with many equipments
    equipments = db.relationship("Equipment", backref="centrals", lazy=True)
    
    #Relatinship between centrals and a unit
    unit_id = db.Column(db.Integer, db.ForeignKey('units.id'), nullable=False)

    # Managment Responsabilities:
    managed_by = db.relationship("Manager",lazy='dynamic')

    
    # Central Identification:
    name = db.Column(db.String(64), unique=True, nullable=False)
    english_name = db.Column(db.String(64), unique=True, nullable=False)
    site = db.Column(db.String(256), nullable=False)
    email = db.Column(db.String(64), unique=True, nullable=False)
    # departamentos 
    postal_code = db.Column(db.String(10), nullable=False)
    address = db.Column(db.String(256), nullable=False)
    city = db.Column(db.String(32), nullable=False)
    state = db.Column(db.String(32), nullable=False)
    phone_1 = db.Column(db.String(16), nullable=False)
    phone_2 = db.Column(db.String(16), nullable=True)
    # map_address = db.Column(db.String(256), nullable=False)
    # photo = db.Column()
    presentation = db.Column(db.String(1024), nullable=False)
    english_presentation = db.Column(db.String(1024), nullable=False)
    services = db.Column(db.String(512), nullable=False)
    attendance_hours = db.Column(db.String(256), nullable=False)
    observations = db.Column(db.String(512), nullable=False)

    # Financial Managment:
    fusp = db.Column(db.Boolean, nullable=False)
    unit_treasury = db.Column(db.Boolean, nullable=False)
    foundation_zerbini = db.Column(db.Boolean, nullable=False)
    foundation_facul_medicina = db.Column(db.Boolean, nullable=False)
    foundation_carlos_alberto_vanzolini = db.Column(db.Boolean, nullable=False)
    foundation_centro_tec_hidraulica = db.Column(db.Boolean, nullable=False)
    foundation_desenvolvimento_tec_eng = db.Column(db.Boolean, nullable=False)
    foundation_apoio_ensino = db.Column(db.Boolean, nullable=False)
    faepa = db.Column(db.Boolean, nullable=False)
    other_foundations = db.Column(db.Boolean, nullable=False)

    # Status:
    aproved = db.Column(db.Boolean, nullable=False)
    pending = db.Column(db.Boolean, nullable=False)

    def __repr__(self):
        return "<User '{}'>".format(self.name)