from .. import db

class EquipmentRelatedRole(db.Model):
    __tablename__ = 'equipment_related_roles'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    person_id = db.Column(db.Integer, db.ForeignKey('people.id'), primary_key=False)
    equipment_id = db.Column(db.Integer, db.ForeignKey('equipments.id'), primary_key=False)
    role = db.Column(db.String(32), nullable=False)