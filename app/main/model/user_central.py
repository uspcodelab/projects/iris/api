from .. import db
import datetime
# Quando um usuario for conectado à uma central, adicionar a hora atual


"""
This file is for the relationship between User and Central:

A user is connected to a central and the User_Central table does that:
A user connected to a central has a especific role inside that central, so we do a connection
between user-central and the role

each role has a unique permission, so we connect role with permission

Entities:
Role
Permission
User-Central ('Manager')

Relationships:
Manager -> Role
Role -> Permission

"""
role_permission = db.Table('role_permission',
    db.Column('role_id', db.Integer, db.ForeignKey('roles.id')),
    db.Column('permission_id', db.Integer, db.ForeignKey('permissions.id'))
)

class Manager(db.Model):
  __tablename__ = 'managers'
  id = db.Column(db.Integer, primary_key=True, autoincrement=True)
  user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
  central_id = db.Column(db.Integer, db.ForeignKey('centrals.id'), nullable=False)
  role_id = db.Column(db.Integer, db.ForeignKey('roles.id'), nullable=False)

class Role(db.Model):
  __tablename__ = 'roles'

  id = db.Column(db.Integer, primary_key=True, autoincrement=True)
  name = db.Column(db.String(64), nullable=False)
  permissions = db.relationship("Permission", secondary=role_permission, lazy='dynamic')
  def __repr__(self):
    return "<Role '{}'>".format(self.name)

class Permission(db.Model):
  __tablename__ = 'permissions'

  id = db.Column(db.Integer, primary_key=True, autoincrement=True)
  name = db.Column(db.String(64), nullable=False, unique=True)
  def __repr__(self):
    return "<Permission '{}'>".format(self.name)
