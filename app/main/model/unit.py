from .. import db

class Unit(db.Model):
    __tablename__ = 'units'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    registered_on = db.Column(db.DateTime(), nullable=False)
    name=db.Column(db.String(100), nullable=False, unique=True)

    #Relationship between departments and a unit
    departments = db.relationship("Department", backref="units", lazy=True)
    
    #Relatinship between centrals and a unit
    centrals = db.relationship("Central", backref="units", lazy=True)

    #Relationship with Person
    person = db.relationship("Person", backref="person", lazy=True, uselist=False)