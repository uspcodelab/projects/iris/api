from .. import db

class Department(db.Model):
    __tablename__ = "departments"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    registered_on = db.Column(db.DateTime(), nullable=False)
    department_name = db.Column(db.String(100), nullable=False, unique=True)

    #Relationship between departments and a unit
    unit_id = db.Column(db.Integer, db.ForeignKey('units.id'), nullable=False)

    #Relationship between equipments and a department
    equipments = db.relationship("Equipment", backref="departments", lazy=True)
