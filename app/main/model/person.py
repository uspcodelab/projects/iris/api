from .. import db

class Person(db.Model):
    __tablename__ = 'people'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    registered_on = db.Column(db.DateTime(), nullable=False)

    name = db.Column(db.String(256), nullable=False)
    email = db.Column(db.String(128), nullable=False, unique=True)

    #Relationship with User
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=True)
    
    #Relationship with Unit
    unit_id = db.Column(db.Integer, db.ForeignKey('units.id'), nullable=True)

    equipments = db.relationship('Equipment', secondary='equipment_related_roles')