import uuid
import datetime

from app.main import db
from app.main.model.person import Person
from app.main.service.unit_service import get_a_unit

def save_new_person(data):
    person = Person.query.filter_by(name=data['person_name']).first()

    if not person:
        person = Person(
            registered_on=datetime.datetime.utcnow(),
            name=data['person_name'],
            email=data['email']
        )
        save_changes(person)

        #Adding relationship with Unit
        try:
            if 'unit_name' in data['unit_name']:
                add_relationship_with_unit(data['unit_name'], data['person_name'])
        except KeyError:
            pass
        response_object = {
            'status': 'success',
            'message': 'Successfully registered.',
        }
        return response_object, 201

def add_relationship_with_unit(unit_name, person_name):
    unit = get_a_unit(name=data['unit_name'])
    person = get_person_by_name(person_name=person_name)  
    person.unit_id = unit.id 
    db.session.commit() 

def get_person_by_email(email):
    return Person.query.filter_by(email=email).first()

def get_person_by_name(person_name):
    return Person.query.filter_by(name=person_name).first()

def save_changes(data):
    db.session.add(data)
    db.session.commit()