import uuid
import datetime

from app.main import db
from app.main.model.user_central import Permission, Role, Manager

def save_new_permission(permission_name):
    permission_exists = Permission.query.filter_by(name=permission_name).first()
    if permission_exists:
        return 'Permission already in database'
    else:
        new_permission = Permission(
            name=permission_name
        )
        save_changes(new_permission)
    

def save_new_role(permissions, role):
    role_exists = Role.query.filter_by(name=role).first()
    if role_exists:
        return 'Role already in database'
    else:
        new_role = Role(name=role)
        save_changes(new_role)
        for permission in permissions:
            perm = Permission.query.filter_by(name=permission).first()
            Role.query.filter_by(name=role).first().permissions.append(perm)
            db.session.commit()

def save_changes(data):
    db.session.add(data)
    db.session.commit()
