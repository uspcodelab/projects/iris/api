import uuid
import datetime

from app.main import db
from app.main.model.central import Central
from app.main.model.unit import Unit
from app.main.service.unit_service import get_a_unit
from app.main.service.manager_service import create_president

import sys

def save_new_central(data):
    central = Central.query.filter_by(name=data['name']).first()
    if central:
        response_object = {
            'status': 'fail',
            'message': 'Central com mesmo nome encontrada',
        }
        return response_object, 409
    
    central = Central.query.filter_by(english_name=data['english_name']).first()
    if central:
        response_object = {
            'status': 'fail',
            'message': 'Central com mesmo nome em inglês encontrada',
        }
        return response_object, 409

    central = Central.query.filter_by(email=data['email']).first()
    if central:
        response_object = {
            'status': 'fail',
            'message': 'Central com mesmo e-mail encontrada',
        }
        return response_object, 409
        
    if not central:
        if 'unit_name' in data:
            data['units'] = get_a_unit(data['unit_name'])
        else:
            data['units'] = None
        if 'phone_2' not in data:
            data['phone_2'] = None
        if 'fusp' in data['finance']:
            data['fusp'] = True
        if 'unit_treasury' in data['finance']:
            data['unit_treasury'] = True
        else:
            data['unit_treasury'] = False
        if 'foundation_zerbini' in data['finance']:
            data['foundation_zerbini'] = True
        else:
            data['foundation_zerbini'] = False
        if 'foundation_facul_medicina' in data['finance']:
            data['foundation_facul_medicina'] = True
        else:
            data['foundation_facul_medicina'] = False
        if 'foundation_carlos_alberto_vanzolini' in data['finance']:
            data['foundation_carlos_alberto_vanzolini'] = True
        else:
            data['foundation_carlos_alberto_vanzolini'] = False
        if 'foundation_centro_tec_hidraulica' in data['finance']:
            data['foundation_centro_tec_hidraulica'] = True
        else:
            data['foundation_centro_tec_hidraulica'] = False
        if 'foundation_desenvolvimento_tec_eng' in data['finance']:
            data['foundation_desenvolvimento_tec_eng'] = True
        else:
            data['foundation_desenvolvimento_tec_eng'] = False
        if 'foundation_apoio_ensino' in data['finance']:
            data['foundation_apoio_ensino'] = True
        else:
            data['foundation_apoio_ensino'] = False
        if 'faepa' in data['finance']:
            data['faepa'] = True
        else:
            data['faepa'] = False
        if 'other_foundations' in data['finance']:
            data['other_foundations'] = True
        else:
            data['other_foundations'] = False

        data['aproved'] = False

        data['pending'] = False

        if 'address_complement' not in data:
            data['address_complement'] = ""

        new_central = Central(

            registered_on=datetime.datetime.utcnow(),

            #Relatinship between centrals and a unit
            units=data['units'],

            # Central Identification:
            name=data['name'],
            english_name=data['english_name'],
            site=data['site'],
            email=data['email'],
            # departamentos
            postal_code=data['postal_code'],
            address="%s, %s, %s" % (data['address'], data['address_number'], data['address_complement'].lower()),
            city=data['city'],
            state=data['state'],
            phone_1=data['phone_1'],
            phone_2=data['phone_2'],
            # map_address=data['map_address'],
            # photo=data['photo'],
            presentation=data['presentation'],
            english_presentation=data['english_presentation'],
            services=data['services'],
            attendance_hours=data['attendance_hours'],
            observations=data['observations'],

            # Financial Managment:
            fusp=data['fusp'],
            unit_treasury=data['unit_treasury'],
            foundation_zerbini=data['foundation_zerbini'],
            foundation_facul_medicina=data['foundation_facul_medicina'],
            foundation_carlos_alberto_vanzolini=data['foundation_carlos_alberto_vanzolini'],
            foundation_centro_tec_hidraulica=data['foundation_centro_tec_hidraulica'],
            foundation_desenvolvimento_tec_eng=data['foundation_desenvolvimento_tec_eng'],
            foundation_apoio_ensino=data['foundation_apoio_ensino'],
            faepa=data['faepa'],
            other_foundations=data['other_foundations'],

            # Status:
            aproved=data['aproved'],
            pending=data['pending']
        )
        save_changes(new_central)
        # after the central was created, we will create its president
        # the president is the user that created the central, we get his ID via data['president_id']
        print(data['president_id'],file=sys.stderr)
        if create_president(user_id=data['president_id'], central_id=new_central.id) != True:
            db.sesssion.delete(new_central)
            db.session.commit()

        response_object = {
            'status': 'success',
            'message': 'Successfully registered.',
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Central already exists. Please Log in.',
        }
        return response_object, 409

def get_all_centrals():
    return Central.query.all()

def get_a_central(name):
    return Central.query.filter_by(name=name).first()


def get_a_central_by_id(id):
    return Central.query.filter_by(id=id).first()


def save_changes(data):
    db.session.add(data)
    db.session.commit()
