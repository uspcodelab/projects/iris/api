import uuid
import datetime

from app.main import db
from app.main.model.user_central import Manager, Role
from app.main.model.user import User
from app.main.model.central import Central

def create_president(user_id, central_id):
  president_id = get_president_role_id() # gets the president role from the DB
  # create a new manager using the parameters and the president role id 
  new_president = Manager(user_id=user_id, central_id=central_id, role_id=president_id)
  save_changes(new_president)
  return True

def create_vice_president(user_id, central_id):
  vice_president_id = get_vice_president_role_id()
  new_vice = Manager(user_id=user_id, central_id=central_id, role_id=vice_president_id)
  save_changes(new_vice)


def get_managers(central_id):
  """ Return a list of managers """
  central = Central.query.filter_by(id=central_id).first()
  return central.managed_by.all()


def get_president_role_id():
  president = Role.query.filter_by(name='President').first()
  return president.id

def get_vice_president_role_id():
  vice_president = Role.query.filter_by(name='Vice-President').first()
  return vice_president.id

def get_president(central_id):
  """ Returns a User object which is the president of the central """
  central = Central.query.filter_by(id=central_id).first() # gets the central
  president = central.managed_by.filter_by(role_id=get_president_role_id()).first() #finds the manager
  user = User.query.filter_by(id=president.user_id).first() # get the user
  return user

def get_vice_president(central_id):
  """ Returns a User object which is the vice president of the central """
  central = Central.query.filter_by(id=central_id).first()
  vice_president = central.managed_by.filter_by(role_id=get_vice_president_role_id()).first()
  if (vice_president):
    user = User.query.filter_by(id=vice_president.user_id).first()
    return user
  return None



def save_changes(manager):
  db.session.add(manager)
  db.session.commit()