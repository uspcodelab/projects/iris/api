from app.main import db
from app.main.model.equipment_related_roles import EquipmentRelatedRole
from app.main.model.person import Person
from app.main.model.equipment import Equipment
from app.main.service.person_service import get_person_by_email

def relate_person_to_equipment(person_email, equipment_id, role):
    person = get_person_by_email(person_email)
    new_relationship = EquipmentRelatedRole(person_id=person.id, equipment_id=equipment_id, role=role)
    save_changes(new_relationship)

def save_changes(relationship_object):
    db.session.add(relationship_object)
    db.session.commit()

def get_responsible(equipment_id):
    person_id = EquipmentRelatedRole.query.filter_by(equipment_id=equipment_id).filter_by(role='responsible').first().person_id
    return Person.query.filter_by(id=person_id).first()

def get_administrator(equipment_id):
    person_id = EquipmentRelatedRole.query.filter_by(equipment_id=equipment_id).filter_by(role='administrator').first().person_id
    return Person.query.filter_by(id=person_id).first()

def get_equipments_under_responsibility(person_id):
    relationship = EquipmentRelatedRole.query.filter_by(person_id=person_id).filter_by(role='responsible').all()
    equipment_array = []
    for element in relationship:
        equipment = Equipment.query.filter_by(id=element.equipment_id).first()
        equipment_array.append(equipment)

    return equipment_array

def get_equipments_under_administration(person_id):
    relationship = EquipmentRelatedRole.query.filter_by(person_id=person_id).filter_by(role='administrator').all()
    equipment_array = []
    for element in relationship:
        equipment = Equipment.query.filter_by(id=element.equipment_id).first()
        equipment_array.append(equipment)

    return equipment_array