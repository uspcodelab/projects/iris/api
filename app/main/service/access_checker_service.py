from flask import request
from app.main.service.user_service import get_a_user
from app.main.service.auth_helper import Auth

def check_central_access(central, request):
    user_id = Auth.get_logged_in_user(request)[0]['data']['user_id']
    user = get_a_user(user_id)
    if user in central.users:
        return True
    else:
        return False