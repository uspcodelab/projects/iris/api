import uuid
import datetime

from app.main import db
from app.main.model.department import Department
from app.main.model.unit import Unit
from app.main.service.unit_service import get_a_unit

def save_new_department(data):
    department = Department.query.filter_by(department_name=data['department_name']).first()
    if not department:
        if 'department_name' not in data:
            data['department_name'] = None
        if 'unit_name' in data:
            data['units'] = get_a_unit(data['unit_name'])
        else:
            data['units'] = None
        
        new_department = Department(

            registered_on=datetime.datetime.utcnow(),
            department_name=data['department_name'],
            units=data['units']
        )

        save_changes(new_department)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered.',
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Department already exists.'
        }
        return response_object, 409

def get_all_departments():
    return Department.query.all()

def get_a_department(department_name):
    return Department.query.filter_by(department_name=department_name).first()

def get_a_department_by_id(id):
    return Department.query.filter_by(id=id).first()

def save_changes(data):
    db.session.add(data)
    db.session.commit()