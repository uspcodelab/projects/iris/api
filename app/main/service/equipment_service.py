import uuid
import datetime

from app.main.service.central_service import get_a_central
from app.main.service.department_service import get_a_department
from app.main.service.person_service import get_person_by_email
from app.main.service.equipment_person_service import relate_person_to_equipment
from app.main.service.person_service import save_new_person
from app.main import db
from app.main.model.equipment import Equipment


def save_new_equipment(data):
    if 'central_name' in data:
        data['centrals'] = get_a_central(data['central_name'])
    else:
        data['centrals'] = None
    if 'equipment_department' in data:
        data['departments'] = get_a_department(data['equipment_department'])
    if data['administrator_is_researcher']:
        data['administrator_unit'] = data['researcher_unit']
        data['administrator_USP_number'] = data['researcher_USP_number']
        data['administrator_name'] = data['researcher_name']
        data['administrator_email'] = data['researcher_email']
    if 'equipment_offered_services' not in data:
        data['equipment_offered_services'] = None
    if 'equipment_applications' not in data:
        data['equipment_applications'] = None
    if 'monday_attendance_time' not in data:
        data['monday_attendance_time'] = None
    if 'tuesday_attendance_time' not in data:
        data['tuesday_attendance_time'] = None
    if 'wednesday_attendance_time' not in data:
        data['wednesday_attendance_time'] = None
    if 'thursday_attendance_time' not in data:
        data['thursday_attendance_time'] = None
    if 'friday_attendance_time' not in data:
        data['friday_attendance_time'] = None
    if 'saturday_attendance_time' not in data:
        data['saturday_attendance_time'] = None
    if 'sunday_attendance_time' not in data:
        data['sunday_attendance_time'] = None
                
    equipment = Equipment.query.filter_by(equipment_name=data['equipment_name']).first()
        
    equipment = Equipment.query.filter_by(equipment_name=data['equipment_name']).first()
        
    time = datetime.datetime.utcnow()
    new_equipment = Equipment(

        # Equipment identification:
        registered_on=time,
        centrals=data['centrals'], 

        equipment_name=data['equipment_name'],
        equipment_english_name=data['equipment_english_name'],
        equipment_presentation=data['equipment_presentation'],
        equipment_brand=data['equipment_brand'],
        equipment_model=data['equipment_model'],
        equipment_patrimony_number=data['equipment_patrimony_number'],
        equipment_acquisition_year=data['equipment_acquisition_year'],
        # equipment_origin

        # Responsable researcher:
        # researcher_unit=data['researcher_unit'],
        # researcher_USP_number=data['researcher_USP_number'],
        # researcher_name=data['researcher_name'],
        # researcher_email=data['researcher_email'],

        # administrator_is_researcher=data['administrator_is_researcher'],

        # Administrator:
        # administrator_unit=data['administrator_unit'],
        # administrator_USP_number=data['administrator_USP_number'],
        # administrator_name=data['administrator_name'],
        # administrator_email=data['administrator_email'],

        # Location:
        departments=data['departments'],
        equipment_address=data['equipment_address'],
        #google maps address required
            
        # General description:
        equipment_description=data['equipment_description'],
        equipment_english_description=data['equipment_english_description'],
        equipment_offered_services=data['equipment_offered_services'],
        equipment_applications=data['equipment_applications'],
        equipment_rules=data['equipment_rules'],

        # Equipment attendance hours:
        monday_attendance_time=data['monday_attendance_time'],
        tuesday_attendance_time=data['tuesday_attendance_time'],
        wednesday_attendance_time=data['wednesday_attendance_time'],
        thursday_attendance_time=data['thursday_attendance_time'],
        friday_attendance_time=data['friday_attendance_time'],
        saturday_attendance_time=data['saturday_attendance_time'],
        sunday_attendance_time=data['sunday_attendance_time']
    )
    save_changes(new_equipment)
    equipment = Equipment.query.filter_by(registered_on=time).first()

    #Adding relationship with the Responsible
    if 'responsible_email' in data:
        responsible = get_person_by_email(data['responsible_email'])
        if not responsible:
            person_data = {
                'person_name': data['responsible_name'],
                'email': data['responsible_email'],
                'unit_name': data['responsible_email'],
                'usp_number': data['responsible_USP_number']
            }
            save_new_person(person_data)
            
        relate_person_to_equipment(data['responsible_email'], equipment.id, 'responsible')
        
    #Adding relationship with the Administrator
    if 'administrator_email' in data:
        administrator = get_person_by_email(data['administrator_email'])
        if not administrator:
            person_data = {
                'person_name': data['administrator_name'],
                'email': data['administrator_email'],
                'unit_name': data['administrator_email'],
                'usp_number': data['administrator_USP_number']
            }
            save_new_person(person_data)

        relate_person_to_equipment(data['administrator_email'], equipment.id, 'administrator')
        
    response_object = {
        'status': 'success',
        'message': 'Successfully registered.',
    }
    return response_object, 201 

def get_all_equipments():
    return Equipment.query.all()

def get_a_equipment(id):
    return Equipment.query.filter_by(id=id).first()

def save_changes(data):
    # data.centrals.pending = True
    db.session.add(data)
    db.session.commit()
