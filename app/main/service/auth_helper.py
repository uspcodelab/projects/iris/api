from app.main.model.user import User
from app.main.service.user_service import get_user_by_email, get_user_email, get_user_name

from flask import request
import sys

class Auth:

    @staticmethod
    def login_user(data):
        try:
            # fetch the user data
            user = get_user_by_email(email=data['user_email'])
            if user and user.check_password(data['password']):
                auth_token = user.encode_auth_token(user.id)
                if auth_token:
                    response_object = {
                        'status': 'success',
                        'message': 'Successfully logged in.',
                        'Authorization': auth_token.decode(),
                        'user' : {
                            'id': user.id,
                            'name': get_user_name(user),
                            'email': get_user_email(user)
                        }
                    }
                    return response_object, 200
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'email or password does not match.'
                }
                return response_object, 401

        except Exception as e:
            # print(e, file=sys.stderr)
            response_object = {
                'status': 'fail',
                'message': 'Try again'
            }
            return response_object, 500

    @staticmethod
    def logout_user(data):
        if data:
            auth_token = data["Authorization"]
        else:
            auth_token = ''
        if auth_token: # if auth_token is not ''
            resp = User.decode_auth_token(auth_token)
            if not isinstance(resp, str): # if resp is not a string
                # mark the token as blacklisted
                try:
                    response_object = {
                        'status': 'success',
                        'message': 'Successfully logged out.'
                    }
                    return response_object, 200
                except Exception as e:
                    response_object = {
                        'status': 'fail',
                        'message': e
                    }
                    return response_object, 200
            else: # if resp is a string, resp decode_auth_token returned an error
                response_object = {
                    'status': 'fail',
                    'message': resp
                }
                return response_object, 401
        else:
            response_object = {
                'status': 'fail',
                'message': 'Provide a valid auth token.'
            }
            return response_object, 403

    @staticmethod
    def get_logged_in_user(request):
        # get the auth token
        auth_token = request.cookies.get('token')
        if auth_token:
            resp = User.decode_auth_token(auth_token)
            if not isinstance(resp, str):
                user = User.query.filter_by(id=resp).first()
                response_object = {
                    'status': 'success',
                }
                return response_object, 200
            response_object = {
                'status': 'fail',
                'message': resp
            }
            return response_object, 401
        else:
            response_object = {
                'status': 'fail',
                'message': 'Provide a valid auth token.'
            }
        return response_object, 401
