import uuid
import datetime

from app.main import db
from app.main.model.user import User
from app.main.service.person_service import get_person_by_email, save_new_person


def save_new_user(data):
    user = get_user_by_email(email=data['email'])

    if not user:

        if 'birth_date' not in data:
            data['birth_date'] = None
        if 'gender' not in data:
            data['gender'] = None
        if 'person_id' not in data:
            data['person_id'] = None
        if 'company_name' not in data:
            data['company_name'] = None
        if 'company_id' not in data:
            data['company_id'] = None
        if 'company_ie' not in data:
            data['company_ie'] = None
        if 'company_im' not in data:
            data['company_im'] = None
        if 'user_phone2' not in data:
            data['user_phone2'] = None

        if 'true' == data['is_USP_user']:
            data['is_USP_user'] = True
        else:
            data['is_USP_user'] = False

        user = User(
            registered_on=datetime.datetime.utcnow(),
            token_id=0,

            # USP Bonding:
            is_USP_user=data['is_USP_user'],
            workplace=data['workplace'],
            USP_bond_options=data['USP_bond_options'],

            # ID Section
            # user_name=data['user_name'],
            user_CPF=data['user_CPF'],
            is_person=data['is_person'],
            birth_date=data['birth_date'],
            gender=data['gender'],
            person_id=data['person_id'],
            company_name=data['company_name'],
            company_id=data['company_id'],
            company_ie=data['company_ie'],
            company_im=data['company_im'],


            # Address
            user_CEP=data['user_CEP'],
            user_address=data['user_address'],
            billing_address=data['billing_address'],
            fields_of_interest=data['fields_of_interest'],
            user_city=data['user_city'],
            user_state=data['user_state'],
            user_phone=data['user_phone'],
            user_phone2=data['user_phone2'],

            # Access data

            # user_email=data['user_email'],
            password=data['password']
        )
        save_changes(user)

        #Checking if the Person is already in the Database
        person = get_person_by_email(email=data['email'])
        if not person:
            save_new_person(data)
        
        user.person = get_person_by_email(email=data['email'])
        db.session.commit()          

        #If successfully registered the user
        return generate_token(user)
    
    else:
        response_object = {
            'status': 'fail',
            'message': 'User already exists',
        }
        return response_object, 409

def get_all_users():
    return User.query.all()

def get_data_from_person(user):
    return {
        'email': user.person.email,
        'name': user.person.name
    }

def get_a_user(id):
    return User.query.filter_by(id=id).first()

def get_user_by_email(email):
    if get_person_by_email(email):
        id = get_person_by_email(email).user_id
        return get_a_user(id)
    else:
        return None

def get_user_email(user):
    return user.person.email

def get_user_name(user):
    return user.person.name

def save_changes(data):
    db.session.add(data)
    db.session.commit()

def generate_token(user):
    try:
        # generate the auth token
        auth_token = user.encode_auth_token(user.id)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered.',
            'Authorization': auth_token.decode()
        }
        return response_object, 201
    except Exception as e:
        print(e)
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401
