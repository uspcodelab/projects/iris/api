import uuid
import datetime
import json

from app.main import db
from app.main.model.unit import Unit
from app.main.model.department import Department

def save_new_unit(data):
    unit = Unit.query.filter_by(name=data['name']).first()
    if not unit:
        if 'name' not in data:
            data['name'] = None
        
        new_unit = Unit(
            registered_on=datetime.datetime.utcnow(),

            name=data['name'],

        )
        save_changes(new_unit)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered.',
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Unit already exists.',
        }
        return response_object, 409

def get_all_units():
    return Unit.query.all()

def get_a_unit(name):
    return Unit.query.filter_by(name=name).first()

def get_a_unit_by_id(id):
    return Unit.query.filter_by(id=id).first()

def get_all_departments_from_unit(unit_name):
    return get_a_unit(name=unit_name).departments

def save_changes(data):
    db.session.add(data)
    db.session.commit()