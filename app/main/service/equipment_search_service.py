import json

from app.main import db
from app.main.model.equipment import Equipment
from app.main.model.central import Central
from app.main.model.unit import Unit

class EquipmentSearchResponse:
    search_term = ''
    # match_equipment_name_size = None
    # match_equipment_english_name_size = None
    # match_equipment_presentation_size = None
    # match_equipment_brand_size = None
    # match_equipment_model_size = None
    # match_equipment_patrimony_number_size = None
    # match_equipment_researcher_name_size = None
    # match_equipment_description_size = None
    # match_equipment_offered_services_size = None
    # match_central_name_size = None
    match_size = None
    equipments = []

def get_matching_equipments(requested_searches):
    search_term = requested_searches['search_term']
    result = EquipmentSearchResponse
    term = search_term
    result.search_term = term
    search_term = '%' + search_term + '%'

    result_list = []

    if 'equipment_name' in requested_searches:
        if requested_searches['equipment_name']:
            querry_result = Equipment.query.filter(Equipment.equipment_name.ilike(search_term))
            
            querry_result = querry_result.order_by(Equipment.equipment_name)
            for i in range (0,querry_result.count()):
                d1 = {'equipment_name': querry_result.all()[i].equipment_name,
                    'presentation': querry_result.all()[i].equipment_presentation,
                    'id': querry_result.all()[i].id
                }
                if d1 not in result_list:
                    result_list.append(d1)

    if 'equipment_english_name' in requested_searches:
        if requested_searches['equipment_english_name']:
            querry_result = Equipment.query.filter(Equipment.equipment_english_name.ilike(search_term))
            
            querry_result = querry_result.order_by(Equipment.equipment_name)
            for i in range (0,querry_result.count()):
                d2 = {'equipment_name': querry_result.all()[i].equipment_name,
                    'presentation': querry_result.all()[i].equipment_presentation,
                    'id': querry_result.all()[i].id
                }
                if d2 not in result_list:
                    result_list.append(d2)


    if 'equipment_presentation' in requested_searches:
        if requested_searches['equipment_presentation']:    
            querry_result = Equipment.query.filter(Equipment.equipment_presentation.ilike(search_term))
            
            querry_result = querry_result.order_by(Equipment.equipment_name)
            for i in range (0,querry_result.count()):
                d3 = {'equipment_name': querry_result.all()[i].equipment_name,
                    'presentation': querry_result.all()[i].equipment_presentation,
                    'id': querry_result.all()[i].id
                }
                if d3 not in result_list:
                    result_list.append(d3)

    if 'equipment_brand' in requested_searches:
        if requested_searches['equipment_brand']:
            querry_result = Equipment.query.filter(Equipment.equipment_brand.ilike(search_term))
            
            querry_result = querry_result.order_by(Equipment.equipment_name)
            for i in range (0,querry_result.count()):
                d4 = {'equipment_name': querry_result.all()[i].equipment_name,
                    'presentation': querry_result.all()[i].equipment_presentation,
                    'id': querry_result.all()[i].id
                }
                if d4 not in result_list:
                    result_list.append(d4)

    if 'equipment_model' in requested_searches:
        if requested_searches['equipment_model']:
            querry_result = Equipment.query.filter(Equipment.equipment_model.ilike(search_term))
            
            querry_result = querry_result.order_by(Equipment.equipment_name)
            for i in range (0,querry_result.count()):
                d5 = {'equipment_name': querry_result.all()[i].equipment_name,
                    'presentation': querry_result.all()[i].equipment_presentation,
                    'id': querry_result.all()[i].id
                }
                if d5 not in result_list:
                    result_list.append(d5)

    if 'equipment_patrimony_number' in requested_searches:
        if requested_searches['equipment_patrimony_number']:
            querry_result = Equipment.query.filter(Equipment.equipment_patrimony_number.ilike(search_term))
            
            querry_result = querry_result.order_by(Equipment.equipment_name)
            for i in range (0,querry_result.count()):
                d6 = {'equipment_name': querry_result.all()[i].equipment_name,
                    'presentation': querry_result.all()[i].equipment_presentation,
                    'id': querry_result.all()[i].id
                }
                if d6 not in result_list:
                    result_list.append(d6)

    if 'equipment_researcher_name' in requested_searches:
        if requested_searches['equipment_researcher_name']:
            querry_result = Equipment.query.filter(Equipment.researcher_name.ilike(search_term))
            
            querry_result = querry_result.order_by(Equipment.equipment_name)
            for i in range (0,querry_result.count()):
                d7 = {'equipment_name': querry_result.all()[i].equipment_name,
                    'presentation': querry_result.all()[i].equipment_presentation,
                    'id': querry_result.all()[i].id
                }
                if d7 not in result_list:
                    result_list.append(d7)


    if 'equipment_presentation' in requested_searches:
        if requested_searches['equipment_presentation']:
            querry_result = Equipment.query.filter(Equipment.equipment_presentation.ilike(search_term))
            
            querry_result = querry_result.order_by(Equipment.equipment_name)
            for i in range (0,querry_result.count()):
                d8 = {'equipment_name': querry_result.all()[i].equipment_name,
                    'presentation': querry_result.all()[i].equipment_presentation,
                    'id': querry_result.all()[i].id
                }
                if d8 not in result_list:
                    result_list.append(d8)

    if 'equipment_offered_services' in requested_searches:
        if requested_searches['equipment_offered_services']:
            querry_result = Equipment.query.filter(Equipment.equipment_offered_services.ilike(search_term))
            
            querry_result = querry_result.order_by(Equipment.equipment_name)
            for i in range (0,querry_result.count()):
                d9 = {'equipment_name': querry_result.all()[i].equipment_name,
                    'presentation': querry_result.all()[i].equipment_presentation,
                    'id': querry_result.all()[i].id
                }
                if d9 not in result_list:
                    result_list.append(d9)

    if 'central_name' in requested_searches:
        if requested_searches['central_name']:
            querry_result = Central.query.filter(Central.name.ilike(search_term))

            for i in range(0, querry_result.count()):
                for j in range(0, len(querry_result.all()[i].equipments)):
                    d10 = {'equipment_name': querry_result.all()[i].equipments[j].equipment_name,
                            'presentation': querry_result.all()[i].equipments[j].equipment_presentation,
                            'id': querry_result.all()[i].equipments[j].id
                        }
                    if d10 not in result_list:
                        result_list.append(d10)


    if 'unit_name' in requested_searches:
        if requested_searches['unit_name']:
            querry_result = Unit.query.filter(Unit.name.ilike(search_term))
            for i in range(0, querry_result.count()):
                for j in range(0, len(querry_result.all()[i].departments)):
                    for k in range(0, len(querry_result.all()[i].departments[j].equipments)):
                        querry_result.all()[i].departments[j].equipments[k]
                        d11 = {'equipment_name': querry_result.all()[i].departments[j].equipments[k].equipment_name,
                            'presentation': querry_result.all()[i].departments[j].equipments[k].equipment_presentation,
                            'id': querry_result.all()[i].departments[j].equipments[k].id
                        }
                        if d11 not in result_list:
                            result_list.append(d11)

    result.equipments = sorted(result_list, key=lambda k: k['equipment_name']) #Reordering everything in the list in alphabetical order
    #return json.dumps(result, ensure_ascii=False).encode('utf8')
    return result