from flask_restplus import Namespace, fields


class CentralDto:
    api = Namespace('central', description='central related operations')
    central = api.model('central', {

        'id': fields.Integer(required=False, description='central Identifier'),
        'registered_on': fields.DateTime,

        'unit_id': fields.Integer(required=False),

        # Managment Responsabilities:
        'president_id': fields.Integer(required=True),

        # Central Identification:
        'name': fields.String,
        'english_name': fields.String,
        'site': fields.String,
        'email': fields.String,
        # departamentos
        'postal_code': fields.Integer,
        'address': fields.String,
        'city': fields.String,
        'state': fields.String,
        'phone_1': fields.String,
        'phone_2': fields.String(default=None, required=False),
        # 'map_address': fields.String,
        # photo
        'presentation': fields.String,
        'english_presentation': fields.String,
        'services': fields.String,
        'attendance_hours': fields.String,
        'observations': fields.String,

        # Financial Managment:
        'fusp': fields.Boolean,
        'unit_treasury': fields.Boolean,
        'foundation_zerbini': fields.Boolean,
        'foundation_facul_medicina': fields.Boolean,
        'foundation_carlos_alberto_vanzolini': fields.Boolean,
        'foundation_centro_tec_hidraulica': fields.Boolean,
        'foundation_desenvolvimento_tec_eng': fields.Boolean,
        'foundation_apoio_ensino': fields.Boolean,
        'faepa': fields.Boolean,
        'other_foundations': fields.Boolean,

        # Status:
        'aproved': fields.Boolean(default=None, required=False),
        'pending':fields.Boolean(default=None, required=False)
    })

class EquipmentDto:
    api = Namespace('equipment', description='equipment related operations')
    equipment = api.model('equipment', {
        'id': fields.Integer(required=False, description='equipment Identifier'),
        'registered_on': fields.DateTime,
        'central_id': fields.Integer(required=False),
        'equipment_name': fields.String(required=True),
        'equipment_english_name': fields.String(required=True),
        'equipment_presentation': fields.String(required=True),
        'equipment_brand': fields.String(required=True),
        'equipment_model': fields.String(required=True),
        'equipment_patrimony_number': fields.String(required=True),
        'equipment_acquisition_year': fields.String(required=True),
        # equipment_origin

        # Responsable researcher:
        'researcher_unit': fields.String(required=True),
        'researcher_USP_number': fields.String(required=True),
        'researcher_name': fields.String(required=True),
        'researcher_email': fields.String(required=True),

        'administrator_is_researcher': fields.Boolean(required=True),

        # Administrator:
        'administrator_unit': fields.String(required=True),
        'administrator_USP_number': fields.String(required=True),
        'administrator_name': fields.String(required=True),
        'administrator_email': fields.String(required=True),

        # Location:
        'department_id': fields.Integer(required=False),
        'equipment_address': fields.String(required=True),
        # google maps address required

        # General description:
        'equipment_description': fields.String(required=True),
        'equipment_english_description': fields.String(required=True),
        'equipment_offered_services': fields.String(default=None, required=False),
        'equipment_applications': fields.String(default=None, required=False),
        'equipment_rules': fields.String(required=True),

        # Equipment attendance hours:
        'monday_attendance_time': fields.String(default=None, required=False),
        'tuesday_attendance_time': fields.String(default=None, required=False),
        'wednesday_attendance_time': fields.String(default=None, required=False),
        'thursday_attendance_time': fields.String(default=None, required=False),
        'friday_attendance_time': fields.String(default=None, required=False),
        'saturday_attendance_time': fields.String(default=None, required=False),
        'sunday_attendance_time': fields.String(default=None, required=False)

    })

class UnitDto:
    api = Namespace('unit', description='unit related operations')
    unit = api.model('unit',{
        'id': fields.Integer(required=False, description='unit identifier'),
        'registered_on': fields.DateTime(description='unit creation time'),
        'name': fields.String(default=None, required=True)
    })

class DepartmentDto:
    api = Namespace('department', description='department related operations')
    department = api.model('unit',{
        'id': fields.Integer(required=False, description='unit identifier'),
        'registered_on': fields.DateTime(description='unit creation time'),
        'department_name': fields.String(default=None, required=True),
        'unit_id': fields.Integer(required=False)
    })

class EquipmentSearchDto:
    api = Namespace('equipment_search', description='equipment search related operations', ordered=False)
    equipment_search = api.model('equipment_search',{
        'search_term': fields.String(required=True),
        'equipment_name': fields.Boolean(default=None),
        'equipment_english_name': fields.Boolean(default=None),
        'equipment_presentation': fields.Boolean(default=None),
        'equipment_brand': fields.Boolean(default=None),
        'equipment_model': fields.Boolean(default=None),
        'equipment_patrimony_number': fields.Boolean(default=None),
        'equipment_researcher_name': fields.Boolean(default=None),
        'equipment_description': fields.Boolean(default=None),
        'equipment_offered_services': fields.Boolean(default=None),
        'central_name': fields.Boolean(default=None),
        'unit_name': fields.Boolean(default=None)
    })
    equipment_fields = {'equipment_name': fields.String(default=None),
                    'presentation': fields.String(default=None),
                    'id': fields.Integer
    }   
    equipment_search_response = api.model('equipment_search_response',{
        'search_term': fields.String(required=True),
        'equipments': fields.List(fields.Nested(equipment_fields))    
    })

class EquipmentSearchDto:
    api = Namespace('equipment_search', description='equipment search related operations', ordered=False)
    equipment_search = api.model('equipment_search',{
        'search_term': fields.String(required=True),
        'equipment_name': fields.Boolean(default=None),
        'equipment_english_name': fields.Boolean(default=None),
        'equipment_presentation': fields.Boolean(default=None),
        'equipment_brand': fields.Boolean(default=None),
        'equipment_model': fields.Boolean(default=None),
        'equipment_patrimony_number': fields.Boolean(default=None),
        'equipment_researcher_name': fields.Boolean(default=None),
        'equipment_description': fields.Boolean(default=None),
        'equipment_offered_services': fields.Boolean(default=None),
        'central_name': fields.Boolean(default=None),
        'unit_name': fields.Boolean(default=None)
    })
    equipment_fields = {'equipment_name': fields.String(default=None),
                    'description': fields.String(default=None),
                    'match_field_result': fields.String(default=None),
                    'match_field': fields.String(default=None)

    }   
    equipment_search_response = api.model('equipment_search_response',{
        'search_term': fields.String(required=True),
        'match_size': fields.Integer(default=0, required=True),
        'match_equipment_name_size': fields.Integer(default=0),
        'match_equipment_english_name_size': fields.Integer(default=0),
        'match_equipment_presentation_size': fields.Integer(default=0),
        'match_equipment_brand_size': fields.Integer(default=0),
        'match_equipment_model_size': fields.Integer(default=0),
        'match_equipment_patrimony_number_size': fields.Integer(default=0),
        'match_equipment_researcher_name_size': fields.Integer(default=0),
        'match_equipment_description_size': fields.Integer(default=0),
        'match_equipment_offered_services_size': fields.Integer(default=0),
        'match_central_name_size': fields.Integer(default=0),
        'match_unit_name_size' : fields.Integer(default=0),
        'equipments': fields.List(fields.Nested(equipment_fields))    
    })

class EquipmentSearchDto:
    api = Namespace('equipment_search', description='equipment search related operations', ordered=False)
    equipment_search = api.model('equipment_search',{
        'search_term': fields.String(required=True),
        'equipment_name': fields.Boolean(default=None),
        'equipment_english_name': fields.Boolean(default=None),
        'equipment_presentation': fields.Boolean(default=None),
        'equipment_brand': fields.Boolean(default=None),
        'equipment_model': fields.Boolean(default=None),
        'equipment_patrimony_number': fields.Boolean(default=None),
        'equipment_researcher_name': fields.Boolean(default=None),
        'equipment_description': fields.Boolean(default=None),
        'equipment_offered_services': fields.Boolean(default=None),
        'central_name': fields.Boolean(default=None),
        'unit_name': fields.Boolean(default=None)
    })
    equipment_fields = {'equipment_name': fields.String(default=None),
                    'presentation': fields.String(default=None),
                    'id': fields.Integer
    }   
    equipment_search_response = api.model('equipment_search_response',{
        'search_term': fields.String(required=True),
        'equipments': fields.List(fields.Nested(equipment_fields))    
    })

class UserDto:
    api = Namespace('user', description='user related operations')
    user = api.model('user', {

        'id': fields.Integer(required=False, description='user Identifier'),
        'registered_on': fields.DateTime,

        # USP Bonding section
        'is_USP_user': fields.Boolean,
        'workplace': fields.String(required=True),
        'USP_bond_options': fields.String(required=True),

        # ID Section
        'user_name': fields.String(required=True),
        'user_CPF': fields.String(required=True),
        'is_person': fields.Boolean(required=True),
        'birth_date': fields.String(required=False),
        'gender': fields.String(required=False),
        'person_id': fields.String(required=False),
        'company_name': fields.String(required=False),
        'company_id': fields.String(required=False),
        'company_ie': fields.String(required=False),
        'company_im': fields.String(required=False),


        # Address
        'user_CEP': fields.String(required=True),
        'user_address': fields.String(required=True),
        'billing_address': fields.String(required=True),
        'fields_of_interest': fields.String(required=True),
        'user_city': fields.String(required=True),
        'user_state': fields.String(required=True),
        'user_phone': fields.String(required=True),
        'user_phone2': fields.String(required=True),


        # Access data
        'user_email': fields.String(required=True),
        'password': fields.String(required=True)
    })

class AuthDto:
    api = Namespace('auth', description='authentication related operations')
    user_auth = api.model('auth_details', {
        'user_email': fields.String(required=True, description='The email address'),
        'password': fields.String(required=True, description='The user password '),
    })