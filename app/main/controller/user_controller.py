from flask import request
from flask_restplus import Resource

from ..util.dto import UserDto
from ..service.user_service import save_new_user, get_all_users, get_a_user
from ..service.manager_service import get_president

api = UserDto.api
_user = UserDto.user


@api.route('/')
class UserList(Resource):
    @api.doc('list_of_registered_users')
    @api.marshal_list_with(_user, envelope='data')
    def get(self):
        """List all registered users"""
        return get_all_users()

    @api.response(201, 'User successfully registered.')
    @api.doc('Create a new user')
    @api.expect(_user)
    def post(self):
        """Creates a new user"""
        data = request.json
        return save_new_user(data=data)


@api.route('/<name>')
@api.param('name', 'The User name')
@api.response(404, 'User not found.')
class User(Resource):
    @api.doc('get a user')
    @api.marshal_with(_user)
    def get(self, name):
        """get a user given its name"""
        user = get_a_user(name)
        if not user:
            api.abort(404)
        else:
            return user
        
@api.route('/<int:id>/president')
@api.param('central id', 'The Central ID')
@api.response(404, 'President not found.')
class User(Resource):
    @api.doc('get a president')
    @api.marshal_with(_user)
    def get(self, id):
        """get a user given its name"""
        president = get_president(id)
        if not president:
            api.abort(404)
        else:
            return president
