from flask import request, make_response, jsonify
from flask_restplus import Resource

from app.main.service.auth_helper import Auth
from ..util.dto import AuthDto

api = AuthDto.api
user_auth = AuthDto.user_auth

@api.route('/login')
class UserLogin(Resource):
    """
        User Login Resource
    """
    @api.doc('user login')
    @api.expect(user_auth, validate=True)
    def post(self):
        # get the post data
        post_data = request.json
        r = Auth.login_user(data=post_data)
        if 'Authorization' not in r[0]:
            return r
        else:
            token = r[0]['Authorization']
            result = jsonify(r[0])
            response = make_response(result)
            response.status_code = r[1]
            response.set_cookie(key='token',value=token)
            return response

@api.route('/logout')
class LogoutAPI(Resource):
    """
    Logout Resource
    """
    @api.doc('logout a user')
    def post(self):
        # get auth token
        data = request.get_json(force=True)
        print(data)
        return Auth.logout_user(data=data)
        #auth_header = request.data['Authorization']
        #return Auth.logout_user(data=auth_header)
