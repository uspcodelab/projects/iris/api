from flask import request
from flask_restplus import Resource

from ..util.dto import EquipmentDto
from ..service.equipment_service import save_new_equipment, get_all_equipments, get_a_equipment

api = EquipmentDto.api
_equipment = EquipmentDto.equipment


@api.route('/')
class EquipmentList(Resource):
    @api.doc('list_of_registered_equipments')
    @api.marshal_list_with(_equipment, envelope='data')
    def get(self):
        """List all registered equipments"""
        return get_all_equipments()

    @api.response(201, 'Equipment successfully created.')
    @api.doc('Create a new equipment')
    @api.expect(_equipment)
    def post(self):
        """Creates a new equipment"""
        data = request.json
        return save_new_equipment(data=data)


@api.route('/<id>')
@api.param('id', 'The Equipment id')
@api.response(404, 'Equipment not found.')
class Equipment(Resource):
    @api.doc('get a equipment')
    @api.marshal_with(_equipment)
    def get(self, id):
        """get a equipment given its name"""
        equipment = get_a_equipment(id)
        if not equipment:
            api.abort(404)
        else:
            return equipment
