from flask import request
from flask_restplus import Resource

from ..util.dto import DepartmentDto
from ..service.department_service import save_new_department, get_all_departments, get_a_department, get_a_department_by_id

api = DepartmentDto.api
_department = DepartmentDto.department

@api.route('/')
class DepartmentList(Resource):
    @api.doc('list_of_registered_departments')
    @api.marshal_list_with(_department, envelope='data')
    def get(self):
        """List all registered departments"""
        return get_all_departments()

    @api.response(201, 'Department successfully created.')
    @api.doc('create a new department')
    @api.expect(_department)
    def post(self):
        """ Creates a new Department """
        data = request.json
        return save_new_department(data=data)


@api.route('/<name>')
@api.param('name', 'The Department name')
@api.response(404, 'Department not found.')
class Department(Resource):
    @api.doc('get a department')
    @api.marshal_with(_department)
    def get(self, name):
        """get a department given its name"""
        department = get_a_department(name)
        if not department:
            api.abort(404)
        else:
            return department

@api.route('/<int:id>')
@api.param('id', 'The Department id')
@api.response(404, 'Department not found.')
class Department(Resource):
    @api.doc('get a department')
    @api.marshal_with(_department)
    def get(self, id):
        """get a department given its id"""
        department = get_a_department_by_id(id)
        if not department:
            api.abort(404)
        else:
            return department

