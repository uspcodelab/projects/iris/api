from flask import request
from flask_restplus import Resource, marshal

from ..util.dto import EquipmentSearchDto
from ..service.equipment_search_service import get_matching_equipments

api = EquipmentSearchDto.api
_equipment_search = EquipmentSearchDto.equipment_search
_equipment_response = EquipmentSearchDto.equipment_search_response

@api.route('/equipments')
class EquipmentSearch(Resource):
    @api.doc('get equipments matching the request')
    @api.expect(_equipment_search)
    @api.response(404, 'Equipment not found')
    def post(self):
        requested_searches = request.json
        r = get_matching_equipments(requested_searches=requested_searches)
        return marshal(r, _equipment_response)