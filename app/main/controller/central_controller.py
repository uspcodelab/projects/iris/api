from flask import request
from flask_restplus import Resource

from ..util.dto import CentralDto
from ..service.central_service import save_new_central, get_all_centrals, get_a_central, get_a_central_by_id
from ..service.manager_service import get_president, get_vice_president, create_vice_president
from ..service.user_service import get_user_by_email

api = CentralDto.api
_central = CentralDto.central


@api.route('/')
class CentralList(Resource):
    @api.doc('list_of_registered_centrals')
    @api.marshal_list_with(_central, envelope='data')
    def get(self):
        """List all registered centrals"""
        return get_all_centrals()

    @api.response(201, 'Central successfully created.')
    @api.doc('create a new central')
    @api.expect(_central)
    def post(self):
        """ Creates a new Central """
        data = request.json
        return save_new_central(data=data)


@api.route('/<name>')
@api.param('name', 'The Central name')
@api.response(404, 'Central not found.')
class Central(Resource):
    @api.doc('get a central')
    @api.marshal_with(_central)
    def get(self, name):
        """get a central given its name"""
        central = get_a_central(name)
        if not central:
            api.abort(404)
        else:
            return central

@api.route('/<int:id>')
@api.param('id', 'The Central id')
@api.response(404, 'Central not found.')
class Central(Resource):
    @api.doc('get a central')
    @api.marshal_with(_central)
    def get(self, id):
        """get a central given its id"""
        central = get_a_central_by_id(id)
        if not central:
            api.abort(404)
        else:
            return central

@api.route('/create_vice')
@api.response(404, 'Vice could not be created.')
class Central(Resource):
    @api.doc('adds a Manager to Vice-President position')
    def post(self):
        """ Receives a payload with central_id and a user email"""
        data = request.json
        print(data)
        user = get_user_by_email(data['email'])
        if not user:
            response_object = {
            'status': 'fail',
            'message': 'O usuário não está cadastrado no sistema.'
            }
            return response_object, 403
        vice = get_vice_president(data['central_id'])
        if vice:
            response_object = {
            'status': 'fail',
            'message': 'A Central já possui Vice-Presidente'
            }
            return response_object, 403
        else:
            create_vice_president(user.id, int(data['central_id']))
            response_object = {
            'status': 'sucess',
            'message': 'Vice-Presidente adicionado com sucesso'
            }
            return response_object, 200
            

