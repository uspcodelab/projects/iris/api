from flask import request
from flask_restplus import Resource

from ..util.dto import UnitDto, DepartmentDto
from ..service.unit_service import save_new_unit, get_all_units, get_a_unit, get_all_departments_from_unit, get_a_unit_by_id

api = UnitDto.api
_unit = UnitDto.unit

@api.route('/')
class UnitList(Resource):
    @api.doc('list_of_registered_units')
    @api.marshal_list_with(_unit, envelope='data')
    def get(self):
        """List all registered units"""
        return get_all_units()

    @api.response(201, 'Unit successfully created.')
    @api.doc('create a new unit')
    @api.expect(_unit)
    def post(self):
        """ Creates a new Unit """
        data = request.json
        return save_new_unit(data=data)


@api.route('/<name>')
@api.param('name', 'The Unit name')
@api.response(404, 'Unit not found.')
class Unit(Resource):
    @api.doc('get a unit')
    @api.marshal_with(_unit)
    def get(self, name):
        """get a unit given its name"""
        unit = get_a_unit(name)
        if not unit:
            api.abort(404)
        else:
            return unit

@api.route('/<int:id>')
@api.param('id', 'The Unit id')
@api.response(404, 'Unit not found.')
class Unit(Resource):
    @api.doc('get a unit')
    @api.marshal_with(_unit)
    def get(self, id):
        """get a unit given its id"""
        unit = get_a_unit_by_id(id)
        if not unit:
            api.abort(404)
        else:
            return unit

@api.route('/<name>/departments')
@api.param('name', 'The Unit name')
@api.response(404, 'No departments found')
class Departments_from_unit(Resource):
    api = DepartmentDto.api
    _department = DepartmentDto.department

    @api.doc('get all departments associated')
    @api.marshal_list_with(_department)
    def get(self, name):
        departments_found = get_all_departments_from_unit(name)
        if not departments_found:
            api.abort(404)
        else:
            return departments_found
