import unittest
import json
from app.test.base import BaseTestCase


def register_user(self):
    return self.client.post(
        '/user/',
        data=json.dumps(dict(
            # USP Bonding:
            is_USP_user=[True],
            workplace='USP',
            USP_bond_options='Graduando',

            # ID Section
            user_name='Teste',
            user_CPF='001.001.001-3',
            is_person=True,
            birth_date='10/10/2010',
            gender='Masculino',
            person_id='001110210',
            company_name='Teste',
            company_id='Teste',
            company_ie='Teste',
            company_im='Teste',


            # Address
            user_CEP='01401001',
            user_address='Rua de Teste,1, São Paulo, SP',
            billing_address='Rua de Teste,1, São Paulo, SP',
            fields_of_interest='Programação',
            user_city='São Paulo',
            user_state='SP',
            user_phone='(11) 91111-1111',
            user_phone2='(11) 91111-1111',

            # Access data

            user_email='meu-email@email.com',
            password='senhaqualquer'
        )),
        content_type='application/json'
    )


def login_user(self):
    return self.client.post(
        '/auth/login',
        data=json.dumps(dict(
            user_email='meu-email@email.com',
            password='senhaqualquer'
        )),
        content_type='application/json'
    )


class TestAuthBlueprint(BaseTestCase):

    def test_registered_user_login(self):
            """ Test for login of registered-user login """
            with self.client:
                # user registration
                user_response = register_user(self)
                response_data = json.loads(user_response.data.decode())
                self.assertTrue(response_data['Authorization'])
                self.assertEqual(user_response.status_code, 201)

                # registered user login
                login_response = login_user(self)
                data = json.loads(login_response.data.decode())
                self.assertTrue(data['Authorization'])
                self.assertEqual(login_response.status_code, 200)

    def test_valid_logout(self):
        """ Test for logout before token expires """
        with self.client:
            # user registration
            user_response = register_user(self)
            response_data = json.loads(user_response.data.decode())
            self.assertTrue(response_data['Authorization'])
            self.assertEqual(user_response.status_code, 201)

            # registered user login
            login_response = login_user(self)
            data = json.loads(login_response.data.decode())
            self.assertTrue(data['Authorization'])
            self.assertEqual(login_response.status_code, 200)

            # valid token logout
            response = self.client.post(
                '/auth/logout',
                headers=dict(
                    Authorization='Bearer ' + json.loads(
                        login_response.data.decode()
                    )['Authorization']
                )
            )
            data = json.loads(response.data.decode())
            self.assertTrue(data['status'] == 'success')
            self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()