from app.main import db
from app.main.service.central_service import save_new_central

def create_user():
    from app.main.service.user_service import save_new_user
    data = {
        'is_USP_user':True,
        'workplace':'Academia',
        'USP_bond_options': 'Graduando',
        'user_CPF':'345454545',
        'birth_date':'19/10/1998',
        'user_CEP': '123456',
        'user_address': 'Rua Usuaria',
        'user_state': 'SP',
        'user_city': 'Votuporanga',
        'user_phone':'123123123',
        'billing_address':'Rua 123',
        'field_of_interest': 'Nada',
        'person_name': 'Pessoa',
        'email': 'teste@teste.com',
        'is_person': True,
        'fields_of_interest':'Nenhum',
        'password': 'teste'
    }
    return save_new_user(data)

def create_central():
    from app.main.service.central_service import save_new_central
    data = {
        'unit_name': "Faculdade de Direito (FD)",
        'name': 'Central_teste',
        'english_name':'Central_teste',
        'site':'http://central.com',
        'email':'central@teste.com',
        'postal_code':'15500022',
        'address':'Avenida Central',
        'address_number':'123',
        'address_complement':'Apartamento 15',
        'city':'Votuporanga',
        'state':'SP',
        'phone_1':'1234567',
        'presentation':'Essa eh a apresentacao da central',
        'english_presentation':'English presentation',
        'services':'Servicos da central',
        'attendance_hours':"12",
        'observations':"observacoes bem observadas",
        'finance':['fusp'],
        'president_id': 1
    }
    return save_new_central(data)

def create_equipment():
    from app.main.service.equipment_service import save_new_equipment
    data = {
    "central_name": "Central_teste",
    "equipment_department": "Direito Civil (DCV)",
    "equipment_name": "Equipamento Teste",
    "equipment_english_name": "Equipment Teste",
    "equipment_presentation": "Blabla",
    "equipment_brand": "blelble",
    "equipment_model": "blilili",
    "equipment_patrimony_number": "1231231",
    "equipment_acquisition_year": "2014",
    "responsible_unit": "Faculdade de Direito (FD)",
    "responsible_USP_number": "213123",
    "responsible_name": "FUlano",
    "responsible_email": "email@email.com",
    "administrator_unit": "Faculdade de Direito (FD)",
    "administrator_USP_number": "213123",
    "administrator_name": "FUlano",
    "administrator_email": "email@email.com",
    "researcher_unit": "Faculdade de Direito (FD)",
    "researcher_USP_number": "213123",
    "researcher_name": "FUlano",
    "researcher_email": "email@email.com",
    "administrator_is_researcher": False,
    "equipment_address": "Endereço do equip",
    "equipment_description": "Descricao do equip",
    "equipment_english_description": "English description",
    "equipment_rules": "equip rules",
    "monday_attendance_time": "teste",
    "tuesday_attendance_time": "teste",
    "wednesday_attendance_time": "teste",
    "thursday_attendance_time": "teste",
    "friday_attendance_time": "teste",
    "saturday_attendance_time": "teste",
    "sunday_attendance_time": "teste"
    }
    return save_new_equipment(data)