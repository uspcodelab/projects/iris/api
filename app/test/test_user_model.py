import unittest
import datetime

from app.main import db
from app.main.model.user import User
from app.test.base import BaseTestCase


class TestUserModel(BaseTestCase):

    def test_encode_auth_token(self):
        user = User(
            registered_on=datetime.datetime.utcnow(),

            # USP Bonding:
            is_USP_user=True,
            workplace='USP',
            USP_bond_options='Graduando',

            # ID Section
            user_name='Teste',
            user_CPF='001.001.001-3',
            is_person=True,
            birth_date='10/10/2010',
            gender='Masculino',
            person_id='001110210',
            company_name='Teste',
            company_id='Teste',
            company_ie='Teste',
            company_im='Teste',


            # Address
            user_CEP='01401001',
            user_address='Rua de Teste,1, São Paulo, SP',
            billing_address='Rua de Teste,1, São Paulo, SP',
            fields_of_interest='Programação',
            user_city='São Paulo',
            user_state='SP',
            user_phone='(11) 91111-1111',
            user_phone2='(11) 91111-1111',

            # Access data

            user_email='meu-email@email.com',
            password='senhaqualquer'
        )
        db.session.add(user)
        db.session.commit()
        auth_token = user.encode_auth_token(user.id)
        self.assertTrue(isinstance(auth_token, bytes))

    def test_decode_auth_token(self):
        user = User(
            registered_on=datetime.datetime.utcnow(),

            # USP Bonding:
            is_USP_user=True,
            workplace='USP',
            USP_bond_options='Graduando',

            # ID Section
            user_name='Teste',
            user_CPF='001.001.001-3',
            is_person=True,
            birth_date='10/10/2010',
            gender='Masculino',
            person_id='001110210',
            company_name='Teste',
            company_id='Teste',
            company_ie='Teste',
            company_im='Teste',


            # Address
            user_CEP='01401001',
            user_address='Rua de Teste,1, São Paulo, SP',
            billing_address='Rua de Teste,1, São Paulo, SP',
            fields_of_interest='Programação',
            user_city='São Paulo',
            user_state='SP',
            user_phone='(11) 91111-1111',
            user_phone2='(11) 91111-1111',

            # Access data

            user_email='meu-email@email.com',
            password='senhaqualquer'
        )
        db.session.add(user)
        db.session.commit()
        auth_token = user.encode_auth_token(user.id)
        self.assertTrue(isinstance(auth_token, bytes))
        self.assertTrue(User.decode_auth_token(auth_token.decode("utf-8") ) == 1)


if __name__ == '__main__':
    unittest.main()