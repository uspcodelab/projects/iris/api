import os
import unittest

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from app.main import create_app, db
from app import blueprint

app = create_app(os.getenv('BOILERPLATE_ENV') or 'dev')
app.register_blueprint(blueprint)

# Declaration of host and port to be used by application
host = os.getenv('HOST')
port = int(os.getenv('PORT'))

app.app_context().push()

manager = Manager(app)

migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)

@manager.command
def run():
    app.run(host=host, port=port)

@manager.command
def test():
    """Runs the unit tests."""
    tests = unittest.TestLoader().discover('app/test', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1

@manager.command
def test_central():
        from app.test.create_data import create_central
        return create_central()

@manager.command
def test_user():
        from app.test.create_data import create_user
        return create_user()

@manager.command
def test_equipment():
        from app.test.create_data import create_equipment
        return create_equipment()

@manager.command
def drop_db():
    db.drop_all()

@manager.command
def create_db():
    db.create_all()

@manager.command
def dump_data():
    dump_units()
    dump_departments()
    dump_permissions()
    dump_roles()
    
# Importing models:
from app.main.model import user
from app.main.model import central
from app.main.model import equipment
from app.main.model import department
from app.main.model import unit
from app.main.model import user_central
from app.main.model import person
from app.main.model import equipment_related_roles

#Importing services:
from app.main.service.unit_service import save_new_unit
from app.main.service.department_service import save_new_department

@manager.command
def dump_units():
    import json
    data = {'name': None}
    f = open('./static/unidades.json')
    parsed_json = json.load(f)
    size = len(parsed_json['unidades'])
    for i in range (0,size):
        data['name'] = parsed_json['unidades'][i]
        save_new_unit(data)

@manager.command
def dump_departments():
    import json
    data = {'department_name': None,
            'unit_name': None}
    f = open('./static/departamentos.json')
    parsed_json = json.load(f)
    size = len(parsed_json['departamentos'])
    for i in range (0,size):
        data['department_name'] = parsed_json['departamentos'][i]['departamento']
        data['unit_name'] = parsed_json['departamentos'][i]['unidade']
        save_new_department(data)

@manager.command
def dump_roles():
    from app.main.service.user_central_service import save_new_role
    import json
    f = open('./static/roles.json')
    parsed_json = json.load(f)
    for element in parsed_json['roles']:
        save_new_role(element['permissions'], element['role'])

@manager.command
def dump_permissions():
    from app.main.service.user_central_service import save_new_permission
    import json
    f = open('./static/permissions.json')
    parsed_json = json.load(f)
    for permission in parsed_json['permissions']:
        save_new_permission(permission)

if __name__ == '__main__':
    manager.run()